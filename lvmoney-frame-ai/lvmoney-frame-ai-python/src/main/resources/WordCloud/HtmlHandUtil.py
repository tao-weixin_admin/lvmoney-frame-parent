# 解析方法一
from bs4 import BeautifulSoup

# 输入参数为要分析的 html 文件名，返回值为对应的 BeautifulSoup 对象
from RequestUtil import save_to_file


def create_doc_from_filename(filename):
    with open(filename, "r", encoding='utf-8') as f:
        html_content = f.read()
        doc = BeautifulSoup(html_content)
    return doc


def parse(doc, node, idName, className, saveFile):
    post_list = doc.find_all(node, id=idName, class_=className)
    result = ''
    for x in post_list:
        content = x.find_all('p')
        # 通过遍历的方式进一步提取<p>元素
        for y in content:
            # 通过遍历的方式进一步提取<p>元素
            result += y.text + '\n'
    save_to_file(saveFile, result)


# def main():
#     filename = "tips1.html"
#     doc = create_doc_from_filename(filename)
#     parse(doc, 'table', 'myTable', '', 'website.txt')
#
#
# if __name__ == '__main__':
#     main()
