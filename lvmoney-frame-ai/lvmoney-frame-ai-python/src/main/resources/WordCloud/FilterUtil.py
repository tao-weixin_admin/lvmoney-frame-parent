import jieba
import jieba.analyse
import codecs
import re

from Const import ENCODING_UTF8, LIMIT_DEFAULT
from StopWordUtil import stopWordlist
from collections import Counter


# 对句子进行中文分词
def seg_depart(sentence, path):
    sentence_depart = jieba.cut(sentence.strip())
    # 创建一个停用词列表
    stopwords = stopWordlist(path)
    # 输出结果为 outstr
    outstr = ''
    #     去停用词
    for word in sentence_depart:
        if word not in stopwords:
            if word != '\t':
                outstr += word
                outstr += " "
    return outstr


# class WordCounter(object):
def count_from_file(file, path, top_limit=0):
    with codecs.open(file, 'r', ENCODING_UTF8) as f:
        content = f.read()
        # 将多个空格替换为一个空格
        content = re.sub(r'\s+', r' ', content)
        content = re.sub(r'\.+', r' ', content)

    # 去停用词
    content = seg_depart(content, path)
    return count_from_str(content, top_limit=top_limit)


def count_from_str(content, top_limit=0):
    if top_limit <= 0:
        top_limit = LIMIT_DEFAULT
    #     提取文章的关键词
    tags = jieba.analyse.extract_tags(content, topK=LIMIT_DEFAULT)

    words = jieba.cut(content)

    counter = Counter()

    for word in words:
        if word in tags:
            counter[word] += 1

    return counter.most_common(top_limit)
