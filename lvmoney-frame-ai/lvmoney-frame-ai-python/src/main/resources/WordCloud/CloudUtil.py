import jieba
import matplotlib.pyplot as plt
import wordcloud
import codecs

from Const import ENCODING_UTF8


def create_word_cloud(file, fontPath, cloudPic):
    content = codecs.open(file, 'r', ENCODING_UTF8).read()
    #     结巴分词
    wordlist = jieba.cut(content)
    wl = " ".join(wordlist)

    print(wl)
    #     设置词云图
    wc = wordcloud.WordCloud(
        #     设置背景颜色
        background_color='pink',
        #     设置最大显示的词数
        max_words=100,
        #     设置字体路径
        font_path=fontPath,
        height=1200,
        width=1600,
        #     设置字体最大值
        max_font_size=300,
        #     设置有多少种配色方案，即多少种随机生成状态
        random_state=30,
    )

    # 生成词云图
    myword = wc.generate(wl)

    # 展示词云图
    plt.imshow(myword)
    plt.axis("off")
    plt.show()
    wc.to_file(cloudPic)
