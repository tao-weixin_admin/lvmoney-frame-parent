from Const import ENCODING_UTF8


# 加载停用词
# 创建停用词列表
def stopWordlist(path: str):
    stopwords = [line.strip() for line in open(path, encoding=ENCODING_UTF8).readlines()]
    return stopwords
